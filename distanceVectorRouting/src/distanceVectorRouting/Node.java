package distanceVectorRouting;

import java.util.*;

public class Node {
	
	private final int ID;
	private int numOfUpdates;
	private static int dvPacketCount;
	private static int lastNode;
	List<Node> neighbors = new ArrayList<>();
	List<Route> routingTable = new ArrayList<>();
	List<List<Integer>> dvPacket = new ArrayList<>();
	
	Node(int ID, int destination, int cost) {
		this.ID = ID;
		addRoute(destination, cost, destination);
	}
	
	public int getID() {
		return ID;
	}
	
	public void addNeighbor(Node n) {
		neighbors.add(n);
	}
	
	public void addRoute(int d, int c, int nH) {
		routingTable.add(new Route(d,c, nH));
	}
	
	
	//prepare DV packet 
	public void preparePacket(int sH) {
		dvPacket.clear(); //empty dvPacket array 
		int infinity = 100000000; //poison reverse
		for(int i = 0; i < routingTable.size(); i++) {
			if(routingTable.get(i).nextHop == sH) {
				//split horizon with poison reverse
				dvPacket.add(Arrays.asList(routingTable.get(i).destination, infinity));
			}
			else {
				dvPacket.add(Arrays.asList(routingTable.get(i).destination, routingTable.get(i).cost));
			}
		}
	}
	
	//send DV packet to all neighbors
	public void sendPacket() {
		for(int i = 0; i < neighbors.size(); i++) {
			preparePacket(neighbors.get(i).ID);
			neighbors.get(i).recievePacket(ID, dvPacket);
			dvPacketCount++;
		}
	}
	
	public void recievePacket(int source, List<List<Integer>> packet) {
		numOfUpdates = 0;
		int totalCost;
		
		//where is the source node destination in the routing Table?
		int sourcePosition = sourcePostion(source); 
		
		for (int j = 0; j < packet.size(); j++) {
			
			// cost to source + cost to destination
			totalCost = (routingTable.get(sourcePosition).cost + packet.get(j).get(1));
			
			// where the destination in received packet is located in existing routing table
			int destInRT = destinationExist(packet.get(j).get(0));
			
			// if destination doesn't exist in routing table and isn't equal to my node ID then add it
			if((packet.get(j).get(0) != ID) && ( destInRT == -1)) {
				//destination, cost, next hop
				addRoute(packet.get(j).get(0),totalCost, source);
				numOfUpdates++;
				lastNode = ID;
				
			}
			else { 
				// destination already exist in routing table
				// compare packet destination to my Node ID before "and" statement or will be out of bounds
				if((packet.get(j).get(0) != ID) && (totalCost < routingTable.get(destInRT).cost)) {
					//update routingTable cost as (ID + sender) 
					routingTable.get(destInRT).cost = totalCost;
					routingTable.get(destInRT).nextHop = source;
					numOfUpdates++;
					lastNode = ID;
				}
			}
		}
		// uncomment if user wants to see routing table after every round
		//printRoutingTable();
	}
	
	public int destinationExist(int dest) {
		for(int i = 0; i < routingTable.size(); i++) {
			if(routingTable.get(i).destination == dest) {
				return i;
			}
		}
		return -1;
	}
	
	// find position of Sender in routingTable
	public int sourcePostion(int nodeId) {
		int i;
		for(i = 0; i < routingTable.size(); i++) {
			if(nodeId == routingTable.get(i).destination) {
				return i;
				}
			}
			return i;
		} 
	
	public int getUpdates() {
		return numOfUpdates;
	}
	
	public int getDvPacketCount() {
		return dvPacketCount;
	}
	
	public int getLastNode() {
		return lastNode;
	}
	
	// after routing tables have converged....send data
	public void sendData(int destination) {
		int nextHop = 0;
		if( destination == ID) {
			System.out.println("Data at destination - Node: " + ID);
		}
		else {
			System.out.println("Data recieved by Node:  " + ID);
			// find route in routing table and forward
			for(int i = 0; i < routingTable.size(); i++) {
				if(destination == routingTable.get(i).destination) {
					nextHop = routingTable.get(i).nextHop;
				}
			}
			for(int j = 0; j < neighbors.size(); j++) {
				if(nextHop == neighbors.get(j).ID) {
					System.out.println("Data forwarded to Node: " + neighbors.get(j).ID);
					neighbors.get(j).sendData(destination);
					break;
				}
			}
		}
	}

	public void printRoutingTable() {
		System.out.println("Node ID: " + ID);
		System.out.println("Dest    " + "Cost    " + "NextH");
		System.out.println("---------------------------");
		for(int i = 0; i < routingTable.size(); i++) {
			System.out.printf("%3s", routingTable.get(i).destination);
			System.out.printf("%10s", routingTable.get(i).cost);  //right, left
			System.out.printf("%10s\n", routingTable.get(i).nextHop); //right, left
		}
		System.out.println();
	}
	
	 //FOR TESTING
	public void printNeighbors() {
		System.out.println("Neighbors for Node: " + ID);
		for(int i = 0; i < neighbors.size(); i++) {
			System.out.println(neighbors.get(i).ID);
		}
		System.out.println();
	}
	
	//FOR TESTING
	public void printDVPacket( ) {
		System.out.println("Node: " + ID + " Dv Packet: ");
		System.out.println("DV PACKET SIZE: "+ dvPacket.size());
		for(int i = 0; i < dvPacket.size(); i++) {
			System.out.println(dvPacket.get(i).get(0) + " " + dvPacket.get(i).get(1));
		}
	}
}