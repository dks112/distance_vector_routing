package distanceVectorRouting;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Driver {

	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		
		 Scanner userInput = new Scanner(System.in);  // Create a Scanner object
		 
	    //user input for topology table file name and number of rounds
	    System.out.print("Enter topology file name: ");
	    String topoFileName = userInput.next();  // Read user input
	    System.out.print("Enter number of rounds: ");
	    
	    String num = userInput.next();
	    int numOfRounds = Integer.parseInt(num);
	    System.out.println();
	    System.out.println("Reading topology file: " + topoFileName);  // Output filename
	    System.out.println("Number of rounds: " + numOfRounds + "\n");
	    
	    // User chooses route of data after nodes converge
	    System.out.println("Where do you want to send data after convergence? ");
	    System.out.print("Starting Node ID: ");
	    String s = userInput.next();
	    System.out.print("Ending Node ID: ");
	    String e = userInput.next();
	    System.out.println();
	    int startNode = Integer.parseInt(s);
	    int endNode = Integer.parseInt(e);
	    
	    userInput.close();
	    
	    NetworkManager nM = new NetworkManager(numOfRounds, topoFileName);
	    System.out.println("Sending data.........");
	    nM.sendData(startNode, endNode);
	}
}
