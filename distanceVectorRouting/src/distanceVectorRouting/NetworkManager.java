package distanceVectorRouting;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NetworkManager {

	public List<Node> nodeList = new ArrayList<>();
	
	int ID;
	int neighbor;
	int cost;
	int count = 0;
	int nodePosition; // Node position in array
	int neighborPosition; // neighbor position in array
	int numOfRounds;
	
	//read topology table and initialize nodes
	NetworkManager(int numOfRounds, String fileName) throws FileNotFoundException, InterruptedException {
		this.numOfRounds = numOfRounds;
		File topoTable = new File(fileName);
		Scanner readTopo = new Scanner(topoTable);
		
		// read topology file and initialize nodes
		while(readTopo.hasNextInt()) {
			ID = readTopo.nextInt(); // set Node ID	
			neighbor = readTopo.nextInt(); // set neighbor
			cost = readTopo.nextInt(); // set cost
			
			nodePosition = idExists(ID); // check if node already exists
			
			// if Node ID doesn't exist then add it
			if ( nodePosition == -1) { 
				// new node constructor also creates new route
				nodeList.add(new Node(ID, neighbor, cost));
			}
			// if Node already exists then add the route to the table
			else {
				nodeList.get(nodePosition).addRoute(neighbor, cost, neighbor);
			}
			
			// if neighbor Node ID doesn't exist then add it
			neighborPosition = idExists(neighbor);
			
			if (neighborPosition == -1) {
				// new node constructor also creates new route
				nodeList.add(new Node(neighbor, ID, cost));
				// get position of ID & neighbor node from master node list and add each to list of neighbors
				// they are each others neighbors
				nodeList.get(idExists(ID)).addNeighbor(nodeList.get(idExists(neighbor)));
				nodeList.get(idExists(neighbor)).addNeighbor(nodeList.get(idExists(ID)));
			}
			// if Neighbor node already exists then add the route to the table 
			else {
				nodeList.get(neighborPosition).addRoute(ID, cost, ID);
				// get position of ID & neighbor node from master node list and add each to list of neighbors
				// they are each others neighbors
				nodeList.get(idExists(ID)).addNeighbor(nodeList.get(idExists(neighbor)));
				nodeList.get(idExists(neighbor)).addNeighbor(nodeList.get(idExists(ID)));
				
			}
		}
		readTopo.close();
		//Print initial routing tables
		System.out.println("Original routing tables: ");
		printNodes();
		//initiate rounds
		iterateRounds();
	}
	
	// check if  node ID already exist
	public int idExists(int nodeId) {
		for(int i = 0; i < nodeList.size(); i++) {
			if(nodeId == nodeList.get(i).getID()) {
				return i; // array position
				}
			}
			return -1;
		}
	
	public void iterateRounds() {
		for(int i = 0; i < numOfRounds; i++) {
			for(int j = 0; j < nodeList.size(); j++) {
				nodeList.get(j).sendPacket();
			}
			if(isConvergent(i)) {break;}
		}
	}
	
	private boolean isConvergent(int roundNum) {
		int updates = 0;
		for(int i = 0; i < nodeList.size(); i++) {
			// get number of updates for this round
			updates += nodeList.get(i).getUpdates();
		}
		
		if ((updates == 0) && (count == 0)) {
			System.out.println("NODES CONVERGED DURING ROUND #" + roundNum);
			System.out.println("----------------------------------------");
			System.out.println(nodeList.get(0).getDvPacketCount() + " Total DV Packets sent. ");
			System.out.println("Node #" + nodeList.get(0).getLastNode() + " was the last node to converge");
			System.out.println();
			count++; // if count is more than zero than nodes have already converged
			System.out.println("Routing tables after convergence: ");
			printNodes();
			return true;
		}
		return false;
	}
	
	public void sendData(int start, int end) {
		for(int i = 0; i < nodeList.size(); i++) {
			if(start == nodeList.get(i).getID()) {
				nodeList.get(i).sendData(end);
			}
		}
	}
	
	// print nodes and their routing tables
	public void printNodes() {
		for(int i = 0; i < nodeList.size(); i++) {
			nodeList.get(i).printRoutingTable();
			//FOR TESTING
			//nodeList.get(i).printNeighbors();
			//nodeList.get(i).printDVPacket();
		}
	}
}