package distanceVectorRouting;

public class Route {
	
	Route(int destination, int cost, int nextHop) {
		this.destination = destination;
		this.cost = cost;
		this.nextHop = nextHop;
	}
		
	public int destination;
	public int nextHop;
	public int cost;
}
